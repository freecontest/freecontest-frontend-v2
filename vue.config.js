module.exports = {
  devServer: {
    proxy: 'https://api.freecontest.xyz/',
    disableHostCheck: true,
  },
}
