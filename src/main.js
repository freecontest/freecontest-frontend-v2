import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// bootstrap importation
import './assets/css/animate.css'
import 'font-awesome/css/font-awesome.min.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'izitoast/dist/css/iziToast.min.css'
import 'vue-datetime/dist/vue-datetime.css'
import * as uiv from 'uiv'

import VueIziToast from 'vue-izitoast'
Vue.use(uiv)
Vue.use(VueIziToast, {
  timeout: 2000,
})

import VueContentPlaceholders from 'vue-content-placeholders'
Vue.use(VueContentPlaceholders)

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
