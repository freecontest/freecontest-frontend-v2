import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    infinite: 121212,
    passport: {
      logined: false
    },
    RouteChanging: false
  },
  mutations: {
    preloader(state, payload) {
      state.RouteChanging = payload.status
    },
    passport(state, payload) {
      state.passport = payload
    }
  },
  actions: {
    fetchAPI: async (
      { state },
      {
        path,
        method = 'GET',
        params = {},
        headers = {},
        accessToken = undefined
      }
    ) => {
      let authHeader = {}
      if (accessToken || state.passport.logined) {
        accessToken = accessToken || window.localStorage.getItem('access_token')
        authHeader = {
          Authorization: `Bearer ${accessToken}`
        }
      }
      const apiServer =
        process.env.NODE_ENV === 'production'
          ? 'https://api.freecontest.xyz/'
          : ''
      let opts = {
        method,
        credentials: 'include',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          ...headers,
          ...authHeader
        }
      }
      if (method !== 'GET' && params) {
        opts = {
          ...opts,
          body: JSON.stringify(params)
        }
      }
      return fetch(apiServer + path, opts).then(r => r.json())
    }
  },
  getters: {
    myPassport: state => state.passport
  }
})
