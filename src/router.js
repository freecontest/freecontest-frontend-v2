import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Store from './store'

/* import main component */
import Header from './layout/Header.vue'
import Home from './views/Home.vue'
import Contest from './views/Contest.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Profile from './views/Profile.vue'
import Account from './views/settings/Account.vue'
import Avatar from './views/settings/Avatar.vue'
import ContestRegister from './views/contest/Register.vue'
import LoginCMS from './views/contest/LoginCMS.vue'
import NotFound from './views/NotFound.vue'
import Rating from './views/Rating.vue'
import Donation from './views/Donation.vue'
/* end */

/* admin components */
import UserManager from './views/admin/UserManager.vue'
import UpdateUser from './views/admin/UpdateUser.vue'
import CreateContest from './views/admin/CreateContest.vue'
import UpdateContest from './views/admin/UpdateContest.vue'
import CreatePost from './views/admin/CreatePost.vue'
import UpdatePost from './views/admin/UpdatePost.vue'
import CreateDonation from './views/admin/CreateDonation.vue'
import UpdateDonation from './views/admin/UpdateDonation.vue'
/* end */

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      components: {
        header: Header,
        main: Home,
      },
    },
    {
      path: '/contest',
      name: 'Contest',
      components: {
        header: Header,
        main: Contest,
      },
    },
    {
      path: '/login',
      name: 'Login',
      components: {
        main: Login,
      },
    },
    {
      path: '/register',
      name: 'Register',
      components: {
        main: Register,
      },
    },
    {
      path: '/rating',
      name: 'Rating',
      components: {
        header: Header,
        main: Rating,
      },
    },
    {
      path: '/donation',
      name: 'Donation',
      components: {
        header: Header,
        main: Donation,
      },
    },
    {
      path: '/donation/create',
      name: 'CreateDonation',
      components: {
        header: Header,
        main: CreateDonation,
      },
      meta: {
        mustBeAdmin: true,
      },
    },
    {
      path: '/donation/update/:id',
      name: 'UpdateDonation',
      components: {
        header: Header,
        main: UpdateDonation,
      },
      meta: {
        mustBeAdmin: true,
      },
    },
    {
      path: '/contest/create',
      name: 'CreateContest',
      components: {
        header: Header,
        main: CreateContest,
      },
      meta: {
        mustBeAdmin: true,
      },
    },
    {
      path: '/contest/update/:id',
      name: 'UpdateContest',
      components: {
        header: Header,
        main: UpdateContest,
      },
      meta: {
        mustBeAdmin: true,
      },
    },
    {
      path: '/user/manager',
      name: 'UserManager',
      components: {
        header: Header,
        main: UserManager,
      },
      meta: {
        mustBeAdmin: true,
      },
    },
    {
      path: '/user/update/:id',
      name: 'UpdateUser',
      components: {
        header: Header,
        main: UpdateUser,
      },
      meta: {
        mustBeAdmin: true,
      },
    },
    {
      path: '/post/create',
      name: 'CreatePost',
      components: {
        header: Header,
        main: CreatePost,
      },
      meta: {
        mustBeAdmin: true,
      },
    },
    {
      path: '/post/update/:id',
      name: 'UpdatePost',
      components: {
        header: Header,
        main: UpdatePost,
      },
      meta: {
        mustBeAdmin: true,
      },
    },
    {
      path: '/profile/:username',
      name: 'Profile',
      components: {
        header: Header,
        main: Profile,
      },
    },
    {
      path: '/settings/account',
      name: 'AccountSetting',
      components: {
        header: Header,
        main: Account,
      },
    },
    {
      path: '/settings/avatar',
      name: 'AvatarSetting',
      components: {
        header: Header,
        main: Avatar,
      },
    },
    {
      path: '/incontest/register/:id',
      name: 'ContestRegister',
      components: {
        header: Header,
        main: ContestRegister,
      },
    },
    {
      path: '/cmslogin/:id',
      name: 'CMSLogin',
      components: {
        main: LoginCMS,
      },
    },
    {
      path: '*',
      redirect: {
        name: 'Home',
      },
    },
  ],
})

router.beforeEach(async function (to, from, next) {
  Store.commit('preloader', { status: true })
  const accessToken = window.localStorage.getItem('access_token')
  if (accessToken) {
    let me = await router.app.$store.dispatch('fetchAPI', {
      path: '/user/me',
      method: 'GET',
      accessToken,
    })
    if (me.success) {
      Store.commit('passport', { ...me.data, logined: true })
    } else {
      window.localStorage.removeItem('access_token')
      Store.commit('passport', { logined: false })
    }
  } else {
    Store.commit('passport', { logined: false })
  }

  if (to.matched.some((record) => record.meta.mustBeAdmin)) {
    let passport = Store.getters.myPassport
    if (passport.logined && passport.role == 'admin') {
      return next()
    } else {
      return next({
        path: '/',
      })
    }
  } else {
    return next()
  }
})

router.afterEach(async function (to, from) {
  Store.commit('preloader', { status: false })
})

export default router
